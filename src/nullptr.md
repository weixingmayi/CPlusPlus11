#nullptr
nullptr是强类型的空指针，用于替代NULL和0,可以避免不必要的错误。

    void func(int){ cout << "func(int) called." << endl; }
	void func(int*){ cout << "func(int*) called." << endl; }
	void NullPtrTest()
	{
		func(NULL);		//func(int) called.
		func(nullptr);	//func(int*) called.
	}
