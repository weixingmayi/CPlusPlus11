#static_assert
static_assert可以实现编译期检查，用于辅助判断编译错误：

	void StaticAssertTest()
	{
		static_assert(AVALUE < 5, "Error: AVALUE should not be equal or greater than 5");
	}
	error C2338: Error: AVALUE should not be equal or greater than 5