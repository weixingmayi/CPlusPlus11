#Auto
auto关键字被用于当有明确的初始化参数时，可以根据具体参数初始化类型，多用于不需要知道具体参数类型的情况（如果需要知道类型请使用decltype）

	auto a = 5;//int
	auto func = [](int a)->bool{return true;}//(bool*)(int);
	