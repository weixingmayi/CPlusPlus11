#Range based for loop
##形式
	for (declaration: expression) statement

##解释
以上形式将被扩展为类似：

	auto&& __range = expression;
	for (auto __begin = begin-expression,
			__end = end-expression;
		__beign != __end;
		++__begin)
	{
		declaration = *__begin;
		statement;
	}

其中beign-expression和end-expression判断规则为：
	
	1. 如果expression是一个数组，begin-expression = __range, end_experreesion = __range + __bound;
	2. 如果expression是一个类的实例，并且该类定义了begin()和end()成员函数，begin-expression = _range.begin(), end-expression = _range.end();
	3. 否则begin-expression = begin(_range), end-expression = end(_range), 此时将根据参数类型查找相应的begin(),end()函数重载执行。

规则1说明数组可以用该语法遍历， 规则2说明所有实现了标准顺序接口(standard sequence interface)的类都可以进行遍历，包括所有stl容器等。规则3提供了一个机会可以在不使用现有容器的情况下使用该语法的可能。

##实例

### 数组

	int a[] = { 1, 2, 3, 4, 5 };
	for (int i : a)
	{
		cout << i << "\t";
	}
	cout << endl;
 
###stl容器

	map<int, float> intmap;
	intmap[0] = 1.0f;
	intmap[1] = 1.1f;
	intmap[2] = 1.2f;
	intmap[3] = 1.3f;
	intmap[4] = 1.4f;

	for (auto& it : intmap)
	{
		cout << it.first << "\t" << it.second << endl;
	}

###自定义容器类

	class IntVector;
	class IntIter{
	public:
		IntIter(const IntVector* _pVector, int _pos):
			m_pVector(_pVector), m_nPos(_pos){}

		int operator*() const;

		const IntIter& operator++()
		{
			++m_nPos;
			return *this;
		}
		bool operator!=(const IntIter& _iter)const
		{
			return m_nPos != _iter.m_nPos;
		}
	private:
		const IntVector*	m_pVector;
		int					m_nPos;
	};


	class IntVector
	{
	public:
		IntVector(){
			for (int i = 0; i < 10;++i)
			{
				m_data[i] = i;
			}
		};
		~IntVector(){};

	public:
		IntIter begin() const
		{
			return IntIter(this, 0);
		}
		IntIter end() const
		{
			return IntIter(this, 10);
		}
		int		get(int _idx) const{
			return m_data[_idx];
		}
	private:
		int m_data[10];
	};
	
	int IntIter::operator*() const
	{
		return m_pVector->get(m_nPos);
	}
	
	int main()
	{
		IntVector v;
		for (int i:v)
		{
			cout << i << "\t";
		}
		cout << endl;
		return 0;
	}

### 使用begin(), end()函数
	
	class IntVector2;
	class IntIter2{
	public:
		IntIter2(const IntVector2* _pVector, int _pos) :
			m_pVector(_pVector), m_nPos(_pos){}

		int operator*() const;
		const IntIter2& operator++()
		{
			++m_nPos;
			return *this;
		}
		bool operator!=(const IntIter2& _iter)const
		{
			return m_nPos != _iter.m_nPos;
		}
	private:
		const IntVector2*	m_pVector;
		int					m_nPos;
	};
	
	IntIter2 begin(const IntVector2& _v)
	{
		return IntIter2(&_v, 0);
	}
	
	IntIter2 end(const IntVector2& _v)
	{
		return IntIter2(&_v, 10);
	}
	
	class IntVector2
	{
	public:
		IntVector2(){
			for (int i = 0; i < 10; ++i)
			{
				m_data[i] = i + 10;
			}
		};
		~IntVector2(){};
	
	public:
		int		get(int _idx) const{
			return m_data[_idx];
		}
	private:
		int m_data[10];
	};
	
	int IntIter2::operator*() const
	{
		return m_pVector->get(m_nPos);
	}

	int main()
	{
		IntVector2 v;
		for (int i : v)
		{
			cout << i << "\t";
		}
		cout << endl;
		return 0;
	}
