#Strongly typed enums
##问题

+ 枚举项被展开到上一层作用域，容易造成名称冲突

	    enum A
    	{
    		None,
			A1
    	}
		enum B
		{
			None, //redifinition
			B1
		}

+ 不能使用完整的名称标识枚举项
		
		A::None

+ 不能定义枚举类型的实际数据类型
	
	默认为int类型，不能自定义其实际的数据结构，或编译器支持情况不同

+ 枚举与int类型默认可相互转换		 

	导致调用以int为参数的函数时，容易出现错误

##形式
强制类型

	enum class EnumName: underlying type
	{
		Enum1,
		Enum2,
		Enum3,
	};

	EnumName e = EnumName::Enum1;

不再进行隐含的类型转换
	
	int s = EnumName::Enum1 //Error
非强制类型
	
	enum EnumName2: underlying type
	{
		Enum1,
		Enum2,
		Enum3,
	}

前置声明
	
	enum class EnumName: underlying type
	enum EnumName2: underlying type



