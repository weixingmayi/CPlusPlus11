#decltype
decltype用于查询表达式的类型，判断的规则是：

	对于decltype( e )而言，其判别结果受以下条件的影响：
    1. 如果e是一个标识符或者类成员的访问表达式，则decltype(e)就是e所代表的实体的类型。如果没有这种类型或者e是一个重载函数集，那么程序是错误的；
    2. 如果e是一个函数调用或者一个重载操作符调用（忽略e外面的括号），那么decltype(e)就是该函数的返回类型；
    3. 如果e不属于以上所述的情况，则假设e的类型是 T：当e是一个左值时，decltype(e)就是T&；否则（e是一个右值），decltype(e)是T。

##实例1：

	class A{
	public:
		A(){};
		~A(){};
		int a;
		int* func(){ return nullptr; }
	};

	A a;
	int i;
	decltype(1.0)	x1;	//double
	decltype(i)		x2;	//int
	decltype(a.a)	x3;	//int

	decltype(a.func()) x4; //int*

	decltype((1.0)) x5;		//double
	decltype((a.a)) x6 = i;	//int&

##实例2
模态定义过程中无法确定返回值的情况，可以利用auto和decltype定义

	template<typename T, typename U>
	auto AddTemplate(T t, U u)->decltype(t+u)
	{
		return t + u;
	}