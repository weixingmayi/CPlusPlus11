#Final method keyword

final 关键字由于标识类的虚函数已封闭，不能再被子类重写

	class FBase{
	virtual void Func() final {};
	};

	class FDerive : public FBase{
		virtual void Func(){};	//Error: 'FBase::Func': function declared as 'final' cannot be overridden by 'FDerive::Func'
	};