#Lambda表达式
Lambda表达式可以方便的创建匿名函数，主要用于需要定义函数但该函数仅使用一次或很少几次，并且在其他地方不会使用，不需要定义具名函数的情况下。

##表达式定义
	1. [capture](params)mutable exception attribute -> ret {body}
	2. [capture](params) -> ret {body}
	3. [capture](params){body}
	4. [capture]{body}

##应用

