#Override keyword

override关键字用于表示类中一个虚函数是要重写父类的同一个虚函数，用于避免如下错误
	class FBase{
		virtual void Func(){};
	};

	class FDerive : public FBase{
		virtual void Func(int i){};
	};

此时子类的Func不再是覆盖而是父类函数的重载，不论这种情况是有意设计还是无意的错误，都很容易导致最终调用时错误调用，并且造成维护困难，因此很多编程指导书中都建议不要定义虚函数的重载函数。

override关键字可以避免这一情况发生
	
	class FBase{
		virtual void Func(){};
	};
	
	class FDerive : public FBase{
		virtual void Func(int i) override{};
	};
		
	 //error C3668: 'FDerive::Func' : method with override specifier 'override' did not override any base class methods