#Rvalue Reference 和 move

##问题

Rvalue Reference和move是为了解决原有标准中不必要的对象拷贝和临时对象构造。

##形式
X&&标识右值引用，使得右值也可以当作引用传递


##实例

###Move构造函数与move赋值函数
	static int g_count = 0;
	
	class A{
	public:
		A()
		{
			ncount = g_count++;
			cout << "A constructing..." << ncount<<endl; 
		}
		~A()
		{
			cout << "A destructing..." << ncount << endl; 
		}
		A(const A& _a)
		{ 
			ncount = g_count++;
			cout << "A copy constructing..." << ncount <<"	_a: "<<_a.ncount<< endl;
		}
		A(A&& _a){
			ncount = g_count++;
			cout << "A move constructing..." << ncount << "	_a: " << _a.ncount << endl;
		}
	
		A& operator=(const A& _a){
			cout << "A operator = called." << endl;
		}
		A& operator=(const A&& _a){
			cout << "A operator = of RValue called." << endl;
		}
		int ncount;
	};
	
	void RValueTest1()
	{
		vector<A> array;
		array.reserve(4);//防止vector移动内存
		A a, b;
		array.push_back(a);
		array.push_back(std::move(b)); //调用参数为右值引用的函数重载
	}

Output:
	
	A constructing...0
	A constructing...1
	A copy constructing...2 _a: 0
	A move constructing...3 _a: 1
	A destructing...1
	A destructing...0
	A destructing...2
	A destructing...3

由于调用move构造函数和赋值函数时可以明确知道传入参数是作为右值使用，所以在构造和赋值时可以直接将内部指针进行赋值，从而避免深度拷贝。

###返回值优化
	
	A funcA()
	{
	    A a;
	    return a;
	}
	int main()
	{
	    A a = funcA();
	    A&& b = funcA();
	    return 0;
	}

Output:
	A constructing...0
	A move constructing...1 _a:0
	A destructing...0
	A constructing...2
	A move constructing...3 _a:2
	A destructing...2
	A destructing...3
	A destructing...1

说明VS2010编译器已经做了返回值优化
	