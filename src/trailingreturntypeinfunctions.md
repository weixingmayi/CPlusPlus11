#Trailing return type in functions（函数返回类型后置）
##形式
auto function(params)->decltype(expression){statements}

##应用
函数返回类型后置通常用于泛型编程中当返回值需要编译器推断确定时，如
	
	template<typename T, typename U>
	??? mul(T t, U u)
	{
		return t*U;
	}

此时在不确定T和U的情况下不能明确定义返回类型???

如果写成

	template<typename T, typename U>
	decltype(t*u) mul(T t, U u)
	{
		return t*U;
	}

因为编译是从左至右进行，所以不能在定义t和u之前使用，编译不能完成。

如果写成

	template<typename T, typename U>
	decltype(*((T*)0) * (*((U*)0))) mul2(T t, U u)
	{
	    return t*u;
	}

可以编译成功，但返回值太过复杂，不便于阅读

利用函数返回类型后置可以写为：

	template<typename T, typename U>
	auto mul(T t, U u)->decltype(t*u)
	{
	    return t*u;
	}	
	
该写法简洁易读，此处auto为占位符，并且**auto只能在函数返回类型后置中作为函数返回类型**